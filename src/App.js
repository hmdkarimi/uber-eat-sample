import React, { Component } from 'react';
import {BrowserRouter,Route,Switch  } from 'react-router-dom';
import Home from './containers/Home/Home';
import Header from './containers/Header/Header';
import Footer from './containers/Footer/Footer';


class App extends Component {

  render() {
    return (

       
          <BrowserRouter>   
              <div>
                <Header/>                

                  <Switch location={this.props.location}>
                     
                        {/* HOME PAGE */}
                        <Route exact  path="/" component={Home}/> 
                      
                  </Switch>  
                                                             
                <Footer/>
                </div>
              
              
          </BrowserRouter>
  
     
    );
  }
}

export default App;
